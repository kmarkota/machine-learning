# # import numpy as np
# # import pandas as pd
# # import matplotlib.pyplot as plt
# # import os
# # import cv2
# #
# # DATADIR = "C:/Users/Kresimir Markota/Desktop/ML/Projekt Pokedex/izdvojeniDataset"
# # CATEGORIES = ["Charmander", "Pikachu","Mewtwo","Bulbasaur","Squirtle"]
# #
# #
# # for category in CATEGORIES:
# #     path = os.path.join(DATADIR,category)
# #     for img in os.listdir(path):
# #         img_array = cv2.imread(os.path.join(path,img))
# #         # plt.imshow(img_array, cmap='gray')
# #         # plt.show()
# #         break
# #     break
# #
# # IMG_SIZE=96
# #
# # new_array= cv2.resize(img_array,(IMG_SIZE,IMG_SIZE))
# # # plt.imshow(new_array,cmap='gray')
# # # plt.show()
# #
# # training_data = []
# #
# # def create_training_data():
# #     for category in CATEGORIES:
# #         path = os.path.join(DATADIR, category)
# #         class_num = CATEGORIES.index(category)
# #         for img in os.listdir(path):
# #             try:
# #                 img_array = cv2.imread(os.path.join(path, img))
# #                 new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
# #                 training_data.append([new_array,class_num])
# #             except Exception as e:
# #                 pass
# #
# # create_training_data()
# #
# # # print(len(training_data))
# #
# # import random
# #
# # random.shuffle(training_data)
# #
# # # for sample in training_data[:10]:
# # #     print(sample[1])
# #
# # X = []
# # y = []
# #
# # for features, labels in training_data:
# #     X.append(features)
# #     y.append(labels)
# #
# #
# # X= np.array(X).reshape(-1,IMG_SIZE,IMG_SIZE, 3)
# #
# #
# # import  pickle
# #
# # pickle_out = open("X.pickle","wb")
# # pickle.dump(X, pickle_out)
# # pickle_out.close()
# #
# # pickle_out = open("y.pickle","wb")
# # pickle.dump(y, pickle_out)
# # pickle_out.close()
#
#
#
#
#
# # import tensorflow as tf
# # import pickle
# # from tensorflow.keras.models import Sequential
# # from tensorflow.keras.layers import Dense,Activation,Dropout,Flatten,Conv2D,MaxPooling2D
# # from tensorflow.keras.callbacks import TensorBoard
# # import time
# #
# # # NAME = "Cats-vs-dog-cnn-64X2-{}".format(int(time.time()))
# # #
# # # TB = TensorBoard(log_dir='logs/{}'.format(NAME))
# #
# #
# # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
# # sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
# #
# #
# # pickle_in = open("X.pickle","rb")
# # X= pickle.load(pickle_in)
# #
# # pickle_in = open("y.pickle","rb")
# # y= pickle.load(pickle_in)
# #
# # X=X/255.0
# #
# #
# # # dense_layers = [0,1,2]
# # # layer_sizes =[16,32,64,128]
# # # conv_layers = [1,2,3]
# # #
# # # for dense_layer in dense_layers:
# # #     for layer_size in layer_sizes:
# # #         for conv_layer in conv_layers:
# # # NAME="{}-conv-{}-nodes-{}-dense-{}".format(conv_layer,layer_size,dense_layer, int(time.time()))
# # # print(NAME)
# # model =Sequential()
# #
# # model.add(Conv2D(64,(3,3), input_shape= X.shape[1:]))
# # model.add(Activation("relu"))
# # model.add(MaxPooling2D(pool_size=(2,2)))
# #
# # # for l in range(conv_layer-1):
# #
# # model.add(Conv2D(64,(3,3)))
# # model.add(Activation("relu"))
# # model.add(MaxPooling2D(pool_size=(2,2)))
# #
# # model.add(Flatten())
# # # for l in range(dense_layer):
# # model.add(Dense(64))
# # model.add(Activation("relu"))
# #
# #
# #
# #
# # model.add(Dense(1))
# # model.add(Activation('sigmoid'))
# #
# #
# # model.compile(loss="binary_crossentropy",
# #               optimizer='adam',
# #               metrics=['accuracy'])
# #
# # model.fit(X,y,batch_size=32,epochs=8, validation_split=0.2)
#
#
#
#
#
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import pickle
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Activation,Dropout,Flatten,Conv2D,MaxPooling2D,BatchNormalization
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras import backend as K
import time

# NAME = "Cats-vs-dog-cnn-64X2-{}".format(int(time.time()))
#
# TB = TensorBoard(log_dir='logs/{}'.format(NAME))


gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


aug = ImageDataGenerator(rotation_range=25, width_shift_range=0.1,
	height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
	horizontal_flip=True, fill_mode="nearest")

pickle_in = open("X.pickle","rb")
X= pickle.load(pickle_in)

pickle_in = open("y.pickle","rb")
y= pickle.load(pickle_in)

X=X/255.0
(trainX, testX, trainY, testY) = train_test_split(X,
	y, test_size=0.2, random_state=42)


# dense_layers = [0,1,2]
# layer_sizes =[16,32,64,128]
# conv_layers = [1,2,3]
#
# for dense_layer in dense_layers:
#     for layer_size in layer_sizes:
#         for conv_layer in conv_layers:
# NAME="{}-conv-{}-nodes-{}-dense-{}".format(conv_layer,layer_size,dense_layer, int(time.time()))
# print(NAME)
model =Sequential()

input_shape=(96,96,3)
chanDim = -1

if K.image_data_format() == "channels_first":
    input_shape = (96,96,3)
    chanDim = 1



model.add(Conv2D(32, (3, 3), padding="same",
                             input_shape=input_shape))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(MaxPooling2D(pool_size=(3, 3)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(Conv2D(64, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))


model.add(Conv2D(128, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(Conv2D(128, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(1024))
model.add(Activation("relu"))
model.add(BatchNormalization())
model.add(Dropout(0.5))

# softmax classifier
model.add(Dense(5))
model.add(Activation("softmax"))
model.compile(loss="sparse_categorical_crossentropy",
              optimizer='adam',
              metrics=['accuracy'])

H = model.fit_generator(
	aug.flow(trainX, trainY, batch_size=32),
	validation_data=(testX, testY),
	steps_per_epoch=32 ,
	epochs=50, verbose=1)

# H=model.fit(aug.flow(trainX,trainY),batch_size=32,epochs=40, validation_split=0.3)

print("[INFO] serializing network...")
model.save("pokemon_model_2")

# save the label binarizer to disk
# print("[INFO] serializing label binarizer...")
# f = open(args["labelbin"], "wb")
# f.write(pickle.dumps(lb))
# f.close()

plt.style.use("ggplot")
plt.figure()
N = 40
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_acc"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="upper left")
plt.savefig("plot")


# import cv2
# import tensorflow as tf
# from tensorflow import keras
#
# CATEGORIES = ["Charmander", "Pikachu","Mewtwo","Bulbasaur","Squirtle"]
#
# def prepare(filepath):
#     IMG_SIZE=96
#     img_array=cv2.imread(filepath)
#     new_array=cv2.resize(img_array,(IMG_SIZE,IMG_SIZE))
#     return new_array.reshape(-1,IMG_SIZE,IMG_SIZE,3)
#
# model = keras.models.load_model("pokemon_model")
#
# prediction = model.predict([prepare("00000111.jpg")])
# print(CATEGORIES[int(prediction[0][0])])